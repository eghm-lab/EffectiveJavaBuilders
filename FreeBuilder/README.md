# FreeBuilder
https://github.com/google/FreeBuilder

* Created project using: mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
* Added FreeBuilder and Guava to my-app/pom.xml
* Create Person.java Interface per FreeBuilder documentation
* mvn compile
* See target/classes/com/mycompany/app/Person_Builder.java

