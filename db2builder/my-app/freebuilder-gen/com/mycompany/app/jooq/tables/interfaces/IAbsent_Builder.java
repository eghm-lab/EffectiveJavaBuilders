// Autogenerated code. Do not modify.
package com.mycompany.app.jooq.tables.interfaces;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.EnumSet;
import javax.annotation.Generated;

/**
 * Auto-generated superclass of {@link IAbsent.Builder},
 * derived from the API of {@link IAbsent}.
 */
@Generated("org.inferred.freebuilder.processor.CodeGenerator")
abstract class IAbsent_Builder {

  private static final Joiner COMMA_JOINER = Joiner.on(", ").skipNulls();

  private enum Property {
    EMPID("empid"),
    ABSID("absid"),
    ABSDATE("absdate"),
    ABSDESCRIPTION("absdescription"),
    ABSNOTES("absnotes"),
    ABSDELETED("absdeleted"),
    ;

    private final String name;

    private Property(String name) {
      this.name = name;
    }

    @Override
    public String toString() {
      return name;
    }
  }

  private String empid;
  private String absid;
  private Timestamp absdate;
  private String absdescription;
  private String absnotes;
  private Boolean absdeleted;
  private final EnumSet<IAbsent_Builder.Property> _unsetProperties =
      EnumSet.allOf(IAbsent_Builder.Property.class);

  /**
   * Sets the value to be returned by {@link IAbsent#getEmpid()}.
   *
   * @return this {@code Builder} object
   * @throws NullPointerException if {@code empid} is null
   */
  public IAbsent.Builder setEmpid(String empid) {
    this.empid = Preconditions.checkNotNull(empid);
    _unsetProperties.remove(IAbsent_Builder.Property.EMPID);
    return (IAbsent.Builder) this;
  }

  /**
   * Returns the value that will be returned by {@link IAbsent#getEmpid()}.
   *
   * @throws IllegalStateException if the field has not been set
   */
  public String getEmpid() {
    Preconditions.checkState(
        !_unsetProperties.contains(IAbsent_Builder.Property.EMPID), "empid not set");
    return empid;
  }

  /**
   * Sets the value to be returned by {@link IAbsent#getAbsid()}.
   *
   * @return this {@code Builder} object
   * @throws NullPointerException if {@code absid} is null
   */
  public IAbsent.Builder setAbsid(String absid) {
    this.absid = Preconditions.checkNotNull(absid);
    _unsetProperties.remove(IAbsent_Builder.Property.ABSID);
    return (IAbsent.Builder) this;
  }

  /**
   * Returns the value that will be returned by {@link IAbsent#getAbsid()}.
   *
   * @throws IllegalStateException if the field has not been set
   */
  public String getAbsid() {
    Preconditions.checkState(
        !_unsetProperties.contains(IAbsent_Builder.Property.ABSID), "absid not set");
    return absid;
  }

  /**
   * Sets the value to be returned by {@link IAbsent#getAbsdate()}.
   *
   * @return this {@code Builder} object
   * @throws NullPointerException if {@code absdate} is null
   */
  public IAbsent.Builder setAbsdate(Timestamp absdate) {
    this.absdate = Preconditions.checkNotNull(absdate);
    _unsetProperties.remove(IAbsent_Builder.Property.ABSDATE);
    return (IAbsent.Builder) this;
  }

  /**
   * Returns the value that will be returned by {@link IAbsent#getAbsdate()}.
   *
   * @throws IllegalStateException if the field has not been set
   */
  public Timestamp getAbsdate() {
    Preconditions.checkState(
        !_unsetProperties.contains(IAbsent_Builder.Property.ABSDATE), "absdate not set");
    return absdate;
  }

  /**
   * Sets the value to be returned by {@link IAbsent#getAbsdescription()}.
   *
   * @return this {@code Builder} object
   * @throws NullPointerException if {@code absdescription} is null
   */
  public IAbsent.Builder setAbsdescription(String absdescription) {
    this.absdescription = Preconditions.checkNotNull(absdescription);
    _unsetProperties.remove(IAbsent_Builder.Property.ABSDESCRIPTION);
    return (IAbsent.Builder) this;
  }

  /**
   * Returns the value that will be returned by {@link IAbsent#getAbsdescription()}.
   *
   * @throws IllegalStateException if the field has not been set
   */
  public String getAbsdescription() {
    Preconditions.checkState(
        !_unsetProperties.contains(IAbsent_Builder.Property.ABSDESCRIPTION),
        "absdescription not set");
    return absdescription;
  }

  /**
   * Sets the value to be returned by {@link IAbsent#getAbsnotes()}.
   *
   * @return this {@code Builder} object
   * @throws NullPointerException if {@code absnotes} is null
   */
  public IAbsent.Builder setAbsnotes(String absnotes) {
    this.absnotes = Preconditions.checkNotNull(absnotes);
    _unsetProperties.remove(IAbsent_Builder.Property.ABSNOTES);
    return (IAbsent.Builder) this;
  }

  /**
   * Returns the value that will be returned by {@link IAbsent#getAbsnotes()}.
   *
   * @throws IllegalStateException if the field has not been set
   */
  public String getAbsnotes() {
    Preconditions.checkState(
        !_unsetProperties.contains(IAbsent_Builder.Property.ABSNOTES), "absnotes not set");
    return absnotes;
  }

  /**
   * Sets the value to be returned by {@link IAbsent#getAbsdeleted()}.
   *
   * @return this {@code Builder} object
   * @throws NullPointerException if {@code absdeleted} is null
   */
  public IAbsent.Builder setAbsdeleted(Boolean absdeleted) {
    this.absdeleted = Preconditions.checkNotNull(absdeleted);
    _unsetProperties.remove(IAbsent_Builder.Property.ABSDELETED);
    return (IAbsent.Builder) this;
  }

  /**
   * Returns the value that will be returned by {@link IAbsent#getAbsdeleted()}.
   *
   * @throws IllegalStateException if the field has not been set
   */
  public Boolean getAbsdeleted() {
    Preconditions.checkState(
        !_unsetProperties.contains(IAbsent_Builder.Property.ABSDELETED), "absdeleted not set");
    return absdeleted;
  }

  /**
   * Sets all property values using the given {@code IAbsent} as a template.
   */
  public IAbsent.Builder mergeFrom(IAbsent value) {
    setEmpid(value.getEmpid());
    setAbsid(value.getAbsid());
    setAbsdate(value.getAbsdate());
    setAbsdescription(value.getAbsdescription());
    setAbsnotes(value.getAbsnotes());
    setAbsdeleted(value.getAbsdeleted());
    return (IAbsent.Builder) this;
  }

  /**
   * Copies values from the given {@code Builder}.
   * Does not affect any properties not set on the input.
   */
  public IAbsent.Builder mergeFrom(IAbsent.Builder template) {
    // Upcast to access the private _unsetProperties field.
    // Otherwise, oddly, we get an access violation.
    EnumSet<IAbsent_Builder.Property> _templateUnset =
        ((IAbsent_Builder) template)._unsetProperties;
    if (!_templateUnset.contains(IAbsent_Builder.Property.EMPID)) {
      setEmpid(template.getEmpid());
    }
    if (!_templateUnset.contains(IAbsent_Builder.Property.ABSID)) {
      setAbsid(template.getAbsid());
    }
    if (!_templateUnset.contains(IAbsent_Builder.Property.ABSDATE)) {
      setAbsdate(template.getAbsdate());
    }
    if (!_templateUnset.contains(IAbsent_Builder.Property.ABSDESCRIPTION)) {
      setAbsdescription(template.getAbsdescription());
    }
    if (!_templateUnset.contains(IAbsent_Builder.Property.ABSNOTES)) {
      setAbsnotes(template.getAbsnotes());
    }
    if (!_templateUnset.contains(IAbsent_Builder.Property.ABSDELETED)) {
      setAbsdeleted(template.getAbsdeleted());
    }
    return (IAbsent.Builder) this;
  }

  /**
   * Resets the state of this builder.
   */
  public IAbsent.Builder clear() {
    IAbsent_Builder _template = new IAbsent.Builder();
    empid = _template.empid;
    absid = _template.absid;
    absdate = _template.absdate;
    absdescription = _template.absdescription;
    absnotes = _template.absnotes;
    absdeleted = _template.absdeleted;
    _unsetProperties.clear();
    _unsetProperties.addAll(_template._unsetProperties);
    return (IAbsent.Builder) this;
  }

  /**
   * Returns a newly-created {@link IAbsent} based on the contents of the {@code Builder}.
   *
   * @throws IllegalStateException if any field has not been set
   */
  public IAbsent build() {
    Preconditions.checkState(_unsetProperties.isEmpty(), "Not set: %s", _unsetProperties);
    return new IAbsent_Builder.Value(this);
  }

  /**
   * Returns a newly-created partial {@link IAbsent}
   * based on the contents of the {@code Builder}.
   * State checking will not be performed.
   * Unset properties will throw an {@link UnsupportedOperationException}
   * when accessed via the partial object.
   *
   * <p>Partials should only ever be used in tests.
   */
  @VisibleForTesting()
  public IAbsent buildPartial() {
    return new IAbsent_Builder.Partial(this);
  }

  private static final class Value implements IAbsent {
    private final String empid;
    private final String absid;
    private final Timestamp absdate;
    private final String absdescription;
    private final String absnotes;
    private final Boolean absdeleted;

    private Value(IAbsent_Builder builder) {
      this.empid = builder.empid;
      this.absid = builder.absid;
      this.absdate = builder.absdate;
      this.absdescription = builder.absdescription;
      this.absnotes = builder.absnotes;
      this.absdeleted = builder.absdeleted;
    }

    @Override
    public String getEmpid() {
      return empid;
    }

    @Override
    public String getAbsid() {
      return absid;
    }

    @Override
    public Timestamp getAbsdate() {
      return absdate;
    }

    @Override
    public String getAbsdescription() {
      return absdescription;
    }

    @Override
    public String getAbsnotes() {
      return absnotes;
    }

    @Override
    public Boolean getAbsdeleted() {
      return absdeleted;
    }

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof IAbsent_Builder.Value)) {
        return false;
      }
      IAbsent_Builder.Value other = (IAbsent_Builder.Value) obj;
      if (!empid.equals(other.empid)) {
        return false;
      }
      if (!absid.equals(other.absid)) {
        return false;
      }
      if (!absdate.equals(other.absdate)) {
        return false;
      }
      if (!absdescription.equals(other.absdescription)) {
        return false;
      }
      if (!absnotes.equals(other.absnotes)) {
        return false;
      }
      if (!absdeleted.equals(other.absdeleted)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      return Arrays.hashCode(
          new Object[] {empid, absid, absdate, absdescription, absnotes, absdeleted});
    }

    @Override
    public String toString() {
      return "IAbsent{"
          + "empid="
          + empid
          + ", "
          + "absid="
          + absid
          + ", "
          + "absdate="
          + absdate
          + ", "
          + "absdescription="
          + absdescription
          + ", "
          + "absnotes="
          + absnotes
          + ", "
          + "absdeleted="
          + absdeleted
          + "}";
    }
  }

  private static final class Partial implements IAbsent {
    private final String empid;
    private final String absid;
    private final Timestamp absdate;
    private final String absdescription;
    private final String absnotes;
    private final Boolean absdeleted;
    private final EnumSet<IAbsent_Builder.Property> _unsetProperties;

    Partial(IAbsent_Builder builder) {
      this.empid = builder.empid;
      this.absid = builder.absid;
      this.absdate = builder.absdate;
      this.absdescription = builder.absdescription;
      this.absnotes = builder.absnotes;
      this.absdeleted = builder.absdeleted;
      this._unsetProperties = builder._unsetProperties.clone();
    }

    @Override
    public String getEmpid() {
      if (_unsetProperties.contains(IAbsent_Builder.Property.EMPID)) {
        throw new UnsupportedOperationException("empid not set");
      }
      return empid;
    }

    @Override
    public String getAbsid() {
      if (_unsetProperties.contains(IAbsent_Builder.Property.ABSID)) {
        throw new UnsupportedOperationException("absid not set");
      }
      return absid;
    }

    @Override
    public Timestamp getAbsdate() {
      if (_unsetProperties.contains(IAbsent_Builder.Property.ABSDATE)) {
        throw new UnsupportedOperationException("absdate not set");
      }
      return absdate;
    }

    @Override
    public String getAbsdescription() {
      if (_unsetProperties.contains(IAbsent_Builder.Property.ABSDESCRIPTION)) {
        throw new UnsupportedOperationException("absdescription not set");
      }
      return absdescription;
    }

    @Override
    public String getAbsnotes() {
      if (_unsetProperties.contains(IAbsent_Builder.Property.ABSNOTES)) {
        throw new UnsupportedOperationException("absnotes not set");
      }
      return absnotes;
    }

    @Override
    public Boolean getAbsdeleted() {
      if (_unsetProperties.contains(IAbsent_Builder.Property.ABSDELETED)) {
        throw new UnsupportedOperationException("absdeleted not set");
      }
      return absdeleted;
    }

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof IAbsent_Builder.Partial)) {
        return false;
      }
      IAbsent_Builder.Partial other = (IAbsent_Builder.Partial) obj;
      if (empid != other.empid && (empid == null || !empid.equals(other.empid))) {
        return false;
      }
      if (absid != other.absid && (absid == null || !absid.equals(other.absid))) {
        return false;
      }
      if (absdate != other.absdate && (absdate == null || !absdate.equals(other.absdate))) {
        return false;
      }
      if (absdescription != other.absdescription
          && (absdescription == null || !absdescription.equals(other.absdescription))) {
        return false;
      }
      if (absnotes != other.absnotes && (absnotes == null || !absnotes.equals(other.absnotes))) {
        return false;
      }
      if (absdeleted != other.absdeleted
          && (absdeleted == null || !absdeleted.equals(other.absdeleted))) {
        return false;
      }
      return _unsetProperties.equals(other._unsetProperties);
    }

    @Override
    public int hashCode() {
      return Arrays.hashCode(
          new Object[] {
            empid, absid, absdate, absdescription, absnotes, absdeleted, _unsetProperties
          });
    }

    @Override
    public String toString() {
      return "partial IAbsent{"
          + COMMA_JOINER.join(
              (!_unsetProperties.contains(IAbsent_Builder.Property.EMPID)
                  ? "empid=" + empid
                  : null),
              (!_unsetProperties.contains(IAbsent_Builder.Property.ABSID)
                  ? "absid=" + absid
                  : null),
              (!_unsetProperties.contains(IAbsent_Builder.Property.ABSDATE)
                  ? "absdate=" + absdate
                  : null),
              (!_unsetProperties.contains(IAbsent_Builder.Property.ABSDESCRIPTION)
                  ? "absdescription=" + absdescription
                  : null),
              (!_unsetProperties.contains(IAbsent_Builder.Property.ABSNOTES)
                  ? "absnotes=" + absnotes
                  : null),
              (!_unsetProperties.contains(IAbsent_Builder.Property.ABSDELETED)
                  ? "absdeleted=" + absdeleted
                  : null))
          + "}";
    }
  }
}
