/**
 * This class is generated by jOOQ
 */
package com.mycompany.app.jooq.tables.pojos;


import com.mycompany.app.jooq.tables.interfaces.IEmpmanager;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.3"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Entity
@Table(name = "empmanager", schema = "iob")
public class Empmanager implements IEmpmanager {

	private static final long serialVersionUID = 655775394;

	private final String empid;
	private final String division;

	public Empmanager(Empmanager value) {
		this.empid = value.empid;
		this.division = value.division;
	}

	public Empmanager(
		String empid,
		String division
	) {
		this.empid = empid;
		this.division = division;
	}

	@Column(name = "empid", nullable = false, length = 36)
	@NotNull
	@Size(max = 36)
	@Override
	public String getEmpid() {
		return this.empid;
	}

	@Column(name = "division", length = 40)
	@Size(max = 40)
	@Override
	public String getDivision() {
		return this.division;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Empmanager (");

		sb.append(empid);
		sb.append(", ").append(division);

		sb.append(")");
		return sb.toString();
	}
}
