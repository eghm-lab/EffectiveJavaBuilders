/**
 * This class is generated by jOOQ
 */
package com.mycompany.app.jooq.tables.pojos;


import com.mycompany.app.jooq.tables.interfaces.IAbsentlog;

import java.sql.Timestamp;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.3"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Entity
@Table(name = "absentlog", schema = "iob")
public class Absentlog implements IAbsentlog {

	private static final long serialVersionUID = -166280553;

	private final String    empid;
	private final String    absid;
	private final Timestamp logdate;
	private final Integer   logversionid;

	public Absentlog(Absentlog value) {
		this.empid = value.empid;
		this.absid = value.absid;
		this.logdate = value.logdate;
		this.logversionid = value.logversionid;
	}

	public Absentlog(
		String    empid,
		String    absid,
		Timestamp logdate,
		Integer   logversionid
	) {
		this.empid = empid;
		this.absid = absid;
		this.logdate = logdate;
		this.logversionid = logversionid;
	}

	@Column(name = "empid", nullable = false, length = 36)
	@NotNull
	@Size(max = 36)
	@Override
	public String getEmpid() {
		return this.empid;
	}

	@Column(name = "absid", nullable = false, length = 36)
	@NotNull
	@Size(max = 36)
	@Override
	public String getAbsid() {
		return this.absid;
	}

	@Column(name = "logdate", nullable = false)
	@NotNull
	@Override
	public Timestamp getLogdate() {
		return this.logdate;
	}

	@Column(name = "logversionid", nullable = false, precision = 8)
	@NotNull
	@Override
	public Integer getLogversionid() {
		return this.logversionid;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Absentlog (");

		sb.append(empid);
		sb.append(", ").append(absid);
		sb.append(", ").append(logdate);
		sb.append(", ").append(logversionid);

		sb.append(")");
		return sb.toString();
	}
}
