/**
 * This class is generated by jOOQ
 */
package com.mycompany.app.jooq.tables.pojos;


import com.mycompany.app.jooq.tables.interfaces.IEmployee;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.3"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Entity
@Table(name = "employee", schema = "iob")
public class Employee implements IEmployee {

	private static final long serialVersionUID = 1734067336;

	private final String empid;
	private final String empuhnumber;
	private final String empfirstname;
	private final String emplastname;
	private final String emprole;
	private final String empdivision;
	private final String empstatus;

	public Employee(Employee value) {
		this.empid = value.empid;
		this.empuhnumber = value.empuhnumber;
		this.empfirstname = value.empfirstname;
		this.emplastname = value.emplastname;
		this.emprole = value.emprole;
		this.empdivision = value.empdivision;
		this.empstatus = value.empstatus;
	}

	public Employee(
		String empid,
		String empuhnumber,
		String empfirstname,
		String emplastname,
		String emprole,
		String empdivision,
		String empstatus
	) {
		this.empid = empid;
		this.empuhnumber = empuhnumber;
		this.empfirstname = empfirstname;
		this.emplastname = emplastname;
		this.emprole = emprole;
		this.empdivision = empdivision;
		this.empstatus = empstatus;
	}

	@Id
	@Column(name = "empid", unique = true, nullable = false, length = 36)
	@NotNull
	@Size(max = 36)
	@Override
	public String getEmpid() {
		return this.empid;
	}

	@Column(name = "empuhnumber", nullable = false, length = 17)
	@NotNull
	@Size(max = 17)
	@Override
	public String getEmpuhnumber() {
		return this.empuhnumber;
	}

	@Column(name = "empfirstname", nullable = false, length = 40)
	@NotNull
	@Size(max = 40)
	@Override
	public String getEmpfirstname() {
		return this.empfirstname;
	}

	@Column(name = "emplastname", nullable = false, length = 40)
	@NotNull
	@Size(max = 40)
	@Override
	public String getEmplastname() {
		return this.emplastname;
	}

	@Column(name = "emprole", nullable = false, length = 1)
	@NotNull
	@Size(max = 1)
	@Override
	public String getEmprole() {
		return this.emprole;
	}

	@Column(name = "empdivision", nullable = false, length = 40)
	@NotNull
	@Size(max = 40)
	@Override
	public String getEmpdivision() {
		return this.empdivision;
	}

	@Column(name = "empstatus", nullable = false, length = 1)
	@NotNull
	@Size(max = 1)
	@Override
	public String getEmpstatus() {
		return this.empstatus;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Employee (");

		sb.append(empid);
		sb.append(", ").append(empuhnumber);
		sb.append(", ").append(empfirstname);
		sb.append(", ").append(emplastname);
		sb.append(", ").append(emprole);
		sb.append(", ").append(empdivision);
		sb.append(", ").append(empstatus);

		sb.append(")");
		return sb.toString();
	}
}
