/**
 * This class is generated by jOOQ
 */
package com.mycompany.app.jooq.tables.pojos;


import com.mycompany.app.jooq.tables.interfaces.IAbsent;

import java.sql.Timestamp;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.3"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Entity
@Table(name = "absent", schema = "iob")
public class Absent implements IAbsent {

	private static final long serialVersionUID = 263886567;

	private final String    empid;
	private final String    absid;
	private final Timestamp absdate;
	private final String    absdescription;
	private final String    absnotes;
	private final Boolean   absdeleted;

	public Absent(Absent value) {
		this.empid = value.empid;
		this.absid = value.absid;
		this.absdate = value.absdate;
		this.absdescription = value.absdescription;
		this.absnotes = value.absnotes;
		this.absdeleted = value.absdeleted;
	}

	public Absent(
		String    empid,
		String    absid,
		Timestamp absdate,
		String    absdescription,
		String    absnotes,
		Boolean   absdeleted
	) {
		this.empid = empid;
		this.absid = absid;
		this.absdate = absdate;
		this.absdescription = absdescription;
		this.absnotes = absnotes;
		this.absdeleted = absdeleted;
	}

	@Column(name = "empid", nullable = false, length = 36)
	@NotNull
	@Size(max = 36)
	@Override
	public String getEmpid() {
		return this.empid;
	}

	@Id
	@Column(name = "absid", unique = true, nullable = false, length = 36)
	@NotNull
	@Size(max = 36)
	@Override
	public String getAbsid() {
		return this.absid;
	}

	@Column(name = "absdate", nullable = false)
	@NotNull
	@Override
	public Timestamp getAbsdate() {
		return this.absdate;
	}

	@Column(name = "absdescription", nullable = false, length = 1)
	@NotNull
	@Size(max = 1)
	@Override
	public String getAbsdescription() {
		return this.absdescription;
	}

	@Column(name = "absnotes")
	@Override
	public String getAbsnotes() {
		return this.absnotes;
	}

	@Column(name = "absdeleted", nullable = false, precision = 1)
	@NotNull
	@Override
	public Boolean getAbsdeleted() {
		return this.absdeleted;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Absent (");

		sb.append(empid);
		sb.append(", ").append(absid);
		sb.append(", ").append(absdate);
		sb.append(", ").append(absdescription);
		sb.append(", ").append(absnotes);
		sb.append(", ").append(absdeleted);

		sb.append(")");
		return sb.toString();
	}
}
